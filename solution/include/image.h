
#include <stdint.h>
#include <stdio.h>
struct image {
	uint64_t width, height;
	struct pixel* data;
};
#pragma pack(push, 1)
struct pixel {
	uint8_t r, g, b;
};
#pragma pack(pop)
