#define padding (4 - (sizeof(struct pixel) * header.biWidth) % 4) % 4

#include "image.h"
#include "bmp.h"
#include "headers.h"
#include <stdlib.h>


enum read_status from_bmp(FILE* in, struct image* image) {
	struct bmp_header header;
	if (!fread(&header, sizeof(struct bmp_header), 1, in))
		return READ_INVALID_HEADER;
	if (header.biCompression != 0 || header.biBitCount != 24 ||
		header.bfReserved != 0 || header.bOffBits != 54 || header.biPlanes != 1 ||
		header.biHeight<1 || header.biWidth<1 || header.biSize != 40)
		return READ_INVALID_BITS;
	if (header.bfType != 0x4D42)
		return READ_INVALID_SIGNATURE;
	image->width = header.biWidth;
	image->height = header.biHeight;
	image->data = malloc(sizeof(struct pixel) * image->height * image->width);
	if (!image->data) return READ_OUT_OF_MEMORY;
	for (uint64_t i = 0; i < image->height; i++) {
		if (fread(&image->data[i * image->width], sizeof(struct pixel), image->width, in)<1) {
			free(&image->data[i * image->width]);
			return READ_INVALID_SIGNATURE;
		}
		fseek(in, padding,SEEK_CUR);
	}
	return READ_OK;
}

enum write_status to_bmp(FILE* out, struct image* image) {
	struct bmp_header header = creat(image);
	if (!fwrite(&header, sizeof(struct bmp_header), 1, out)) {
		return WRITE_ERROR;
	}
	char null[3] = { 0 };
	for (uint64_t i = 0; i < image->height; i++) {
		if (!fwrite(&image->data[i * image->width], sizeof(struct pixel), image->width, out)) {
			return WRITE_ERROR;
		}
		if (fwrite(null, padding, 1, out) != 1) {
			return WRITE_ERROR;
		}
	}
	fwrite(null, padding, 1, out);
	return WRITE_OK;
}
