#include "image.h"
#include "bmp.h"
#include "rotate.h"
#include <stdlib.h>
int main( int argc, char* argv[]) {
    if (argc !=4) {
        return 1;
    }
    struct image img={0};
    FILE *fp= fopen(argv[1], "rb");
    if (fp) {
        from_bmp(fp, &img);
        fclose(fp);
    }
    int angle = atoi(argv[3]);
    if (angle == 0 && argv[3][0] != '0') {
        return 1;
    }
    rotate(&img,angle);
    
    fp = fopen(argv[2], "wb");
    if (fp) {
        to_bmp(fp, &img);
        fclose(fp);
    }
    free(img.data);
    return 0;
}
