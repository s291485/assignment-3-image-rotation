#include "image.h"
#include <stdlib.h>

#include "rotate.h"
void rotate(struct image* source, int angle) {
    if (angle < 0)
        angle = 360 + angle;
    while (angle > 0) {
        rotate2(source);
        angle-=90;
    }
}
void rotate2(struct image* source) {
    struct image rotates = { 0 };

    uint64_t width = source->width;
    uint64_t height = source->height;
    rotates.height = width;
    rotates.width = height;
    rotates.data = malloc((sizeof(struct pixel) * height * width));
    if (rotates.data != NULL) {
        for (uint64_t y = 0; y < height; y++)
        {
            for (uint64_t x = 0; x < width; x++)
            {
                rotates.data[(width - 1 - x) * height + y] = source->data[y * source->width + x];
            }
        }
        if (source->data!=NULL){
            free(source->data);
            source->data=NULL;
        }
        source->height = rotates.height;
        source->width = rotates.width;
        source->data = rotates.data;
    }
}
