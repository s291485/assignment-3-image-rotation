#define Type 0x4D42
#define fileSize (sizebmp + (sizepixel * width + pand) * height)
#define offBits sizeof(struct bmp_header)
#define Size 0x0028
#define Width (uint32_t)image->width
#define Height (uint32_t)image->height
#define Planes 0x0001
#define bitCount 0x0018
#define PelsperMeter 0x00000B12

#include "image.h"
#include "bmp.h"
#include "headers.h"
struct bmp_header creat(struct image* image) {
	struct bmp_header headers = { 0 };
	uint32_t sizepixel = (uint32_t)(sizeof(struct pixel));
	uint32_t width = (uint32_t)image->width;
	uint32_t height = (uint32_t)image->height;
	uint32_t sizebmp = (uint32_t)sizeof(struct bmp_header);
	uint8_t pand = (uint8_t)((4 - (width * sizepixel) % 4) % 4);
	headers.bfType = Type;
	headers.bfileSize = fileSize;
	headers.bOffBits = offBits;
	headers.biSize = Size;
	headers.biWidth = Width;
	headers.biHeight = Height;
	headers.biPlanes = Planes;
	headers.biBitCount = bitCount;
	headers.biXPelsPerMeter = PelsperMeter;
	headers.biYPelsPerMeter = PelsperMeter;
	return headers;
}
